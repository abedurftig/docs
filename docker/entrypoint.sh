#!/bin/sh
# delete default config
rm /etc/nginx/conf.d/default.conf
# add custom config (routing everything to index.html)
cp /tmp/nginx.conf /etc/nginx/conf.d/
nginx -g 'daemon off;'
