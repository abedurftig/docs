# Root

This is my first shot.

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```

## Docker

### Build Image

```
docker build -f docker/Dockerfile -t docs:latest .
```

### Run Container

```
docker run -p 80:8080 docs:latest
```